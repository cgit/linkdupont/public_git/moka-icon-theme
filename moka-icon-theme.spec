%global		debug_package	%{nil}

Name:		moka-icon-theme
Version:	5.3.5
Release:	1%{?dist}
Summary:	Simple and consistent icon set

License:	GPLv3+ or CC-BY-SA
URL:		https://snwh.org/moka
Source0:	https://github.com/snwh/moka-icon-theme/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

Requires:	faba-icon-theme
BuildRequires:	autoconf
BuildRequires:	automake

BuildArch:	noarch


%description
%{summary}.


%prep
%autosetup

%build
NOCONFIGURE=1 ./autogen.sh
%configure
%make_build
chmod a-x README.md


%install
%make_install


%files
%license COPYING
%doc README.md AUTHORS
%{_datadir}/icons/Moka


%changelog
* Sat Nov 26 2016 Link Dupont <linkdupont@fedoraproject.org> - 5.3.5-1
- Initial package
